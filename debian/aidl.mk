NAME = aidl
SOURCES = main_java.cpp
LDFLAGS += -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -L$(OUT_DIR) -laidl-common \
           -L/usr/lib/$(DEB_HOST_MULTIARCH)/android -lbase

build: $(SOURCES)
	mkdir --parents $(OUT_DIR)
	$(CXX) $^ -o $(OUT_DIR)/$(NAME) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)